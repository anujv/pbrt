#pragma once

#include <cassert>

#define ASSERT( X ) assert( ( X ) )
