#pragma once

#include "common.h"
#include "point.h"
#include "vector.h"

namespace pbrt {

  class Medium {};

  class Ray {
  public:
    Ray( void );
    Ray( const Point3f& origin, const Vec3f& direciton, Float max_t = Infinity,
         Float time = 0.0f, const Medium* medium = nullptr );

    Point3f operator()( Float t ) const;

    bool IsNaN( void ) const;
  public:
    Point3f o;
    Vec3f   d;
    mutable Float t_max  = Infinity;
    Float time           = 0.0f;
    const Medium* medium = nullptr;
  };

  static std::ostream& operator<<( std::ostream& os, const Ray& r );

  Ray::Ray( void ) {}
  Ray::Ray( const Point3f& origin, const Vec3f& direciton, Float max_t, Float timer, const Medium* med ) :
    o( origin ), d( direciton ), t_max( max_t ), time( timer ), medium( med ) {}

  inline Point3f Ray::operator()( Float t ) const {
    return o + d * t;
  }

  inline bool Ray::IsNaN( void ) const {
    return ( o.IsNaN() || d.IsNaN() || std::isnan( t_max ) );
  }

  inline std::ostream& pbrt::operator<<( std::ostream& os, const Ray& r ) {
    os << "[Ray: o = " << r.o << ", d = " << r.d << ", t_max = "
       << r.t_max << ", time = " << r.time << "]";
    return os;
  }

  // ray differential

  class RayDifferential : public Ray {
  public:
    RayDifferential( void );
    RayDifferential( const Point3f& origin, const Vec3f& direciton, Float max_t = Infinity,
                     Float time = 0.0f, const Medium* medium = nullptr );
    RayDifferential( const Ray& ray );

    void ScaleDifferentials( Float s );
    bool IsNaN( void ) const;
  public:
    bool has_differential = false;
    Point3f rx_origin, ry_origin;
    Vec3f rx_dir, ry_dir;
  };
  
  RayDifferential::RayDifferential( void ) {}
  RayDifferential::RayDifferential( const Point3f& origin, const Vec3f& direciton, Float max_t,
                                    Float time, const Medium* medium ) :
    Ray( origin, direciton, max_t, time, medium ) {}
  RayDifferential::RayDifferential( const Ray& ray ) :
    Ray( ray ) {}

  inline void RayDifferential::ScaleDifferentials( Float s ) {
    rx_origin = o + ( rx_origin - o ) * s;
    ry_origin = o + ( ry_origin - o ) * s;
    rx_dir = d + ( rx_dir - d ) * s;
    ry_dir = d + ( ry_dir - d ) * s;
  }

  inline bool RayDifferential::IsNaN( void ) const {
    bool hasnan = rx_origin.IsNaN() || ry_origin.IsNaN() || rx_dir.IsNaN() || ry_dir.IsNaN();
    return Ray::IsNaN() || ( has_differential && hasnan );
  }

}
