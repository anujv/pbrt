#pragma once

#include "common.h"

namespace pbrt {

  template<typename T>
  struct Vec2 {
    T x, y;

    // constructors

    Vec2( void );
    Vec2( T _x, T _y );
    Vec2( const Vec2<T>& v );
    explicit Vec2( T v );

    // operators

    Vec2<T>& operator=( const Vec2<T>& v );

    Vec2<T> operator+( const Vec2<T>& v ) const;
    Vec2<T> operator-( const Vec2<T>& v ) const;
    Vec2<T> operator*( const T& f ) const;
    Vec2<T> operator/( const T& f ) const;

    Vec2<T>& operator+=( const Vec2<T>& v );
    Vec2<T>& operator-=( const Vec2<T>& v );
    Vec2<T>& operator*=( const T& f );
    Vec2<T>& operator/=( const T& f );

    Vec2<T> operator-( void ) const;
    T operator[]( uint32_t i ) const;
    T& operator[]( uint32_t i );

    bool operator==( const Vec2<T>& v ) const;
    bool operator!=( const Vec2<T>& v ) const;

    Float LengthSq( void ) const;
    Float Length( void ) const;

    bool IsNaN( void ) const;
  };

  template<typename T>
  inline std::ostream& operator<<( std::ostream& os, const Vec2<T>& v );

  // vec3

  template<typename T>
  struct Normal3;

  template<typename T>
  struct Vec3 {
    T x, y, z;

    // constructors

    Vec3( void );
    Vec3( T _x, T _y, T _z );
    Vec3( const Vec3<T>& v );
    explicit Vec3( T v );
    explicit Vec3( const Normal3<T>& n );

    // operators

    Vec3<T>& operator=( const Vec3<T>& v );

    Vec3<T> operator+( const Vec3<T>& v ) const;
    Vec3<T> operator-( const Vec3<T>& v ) const;
    Vec3<T> operator*( const T& f ) const;
    Vec3<T> operator/( const T& f ) const;

    Vec3<T>& operator+=( const Vec3<T>& v );
    Vec3<T>& operator-=( const Vec3<T>& v );
    Vec3<T>& operator*=( const T& f );
    Vec3<T>& operator/=( const T& f );

    Vec3<T> operator-( void ) const;
    T operator[]( uint32_t i ) const;
    T& operator[]( uint32_t i );

    bool operator==( const Vec3<T>& v ) const;
    bool operator!=( const Vec3<T>& v ) const;

    Float LengthSq( void ) const;
    Float Length( void ) const;

    bool IsNaN( void ) const;
  };

  template<typename T>
  inline std::ostream& operator<<( std::ostream& os, const Vec3<T>& v );

  template<typename T>
  inline Vec3<T> Abs( const Vec3<T>& v );

  template<typename T>
  inline T Dot( const Vec3<T>& v1, const Vec3<T>& v2 );

  template<typename T>
  inline T AbsDot( const Vec3<T>& v1, const Vec3<T>& v2 );

  template<typename T>
  inline Vec3<T> Cross( const Vec3<T>& v1, const Vec3<T>& v2 );

  template<typename T>
  inline Vec3<T> Normalize( const Vec3<T>& v );

  #include "vector.inl"

  using Vec2f = Vec2<Float>;
  using Vec2i = Vec2<int32_t>;
  using Vec3f = Vec3<Float>;
  using Vec3i = Vec3<int32_t>;

}
