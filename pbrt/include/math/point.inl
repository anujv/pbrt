
// point2

#define CHECK_POINT( P ) ASSERT( !(P).IsNaN() )

template<typename T>
inline Point2<T>::Point2( void ) : x( 0 ), y( 0 ) {}

template<typename T>
inline Point2<T>::Point2( T _x, T _y ) : x( _x ), y( _y ) { CHECK_POINT( *this ); }

template<typename T>
inline Point2<T>::Point2( const Point2<T>& p ) : x( p.x ), y( p.y ) { CHECK_POINT( *this ); }

template<typename T>
template<typename U>
inline Point2<T>::Point2( const Point2<U>& p ) { x = (T)p.x; y = (T)p.y; CHECK_POINT( *this ); }

template<typename T>
template<typename U>
inline Point2<T>::Point2( const Vec2<U>& v ) { x = (T)v.x; y = (T)v.y; CHECK_POINT( *this ); }

template<typename T>
inline Point2<T>::Point2( T v ) : x( v ), y( v ) { CHECK_POINT( *this ); }

template<typename T>
inline Point2<T>& Point2<T>::operator=( const Point2<T>& p ) {
  CHECK_POINT( p );
  x = p.x;
  y = p.y;
  return *this;
}

template<typename T>
inline Point2<T> Point2<T>::operator+( const Vec2<T>& v ) const {
  CHECK_VECTOR( v );
  return Point2<T>( x + v.x, y + v.y );
}

template<typename T>
inline Point2<T> Point2<T>::operator+( const Point2<T>& p ) const {
  CHECK_POINT( p );
  return Point2<T>( x + p.x, y + p.y );
}

template<typename T>
inline Vec2<T> Point2<T>::operator-( const Point2<T>& p ) const {
  CHECK_POINT( p );
  return Vec2<T>( x - p.x, y - p.y );
}

template<typename T>
inline Point2<T> Point2<T>::operator-( const Vec2<T>& v ) const {
  CHECK_VECTOR( v );
  return Point2<T>( x - v.x, y - v.y );
}

template<typename T>
inline Point2<T> Point2<T>::operator*( const T& f ) const {
  ASSERT( !std::isnan( f ) );
  return Point2<T>( x * f, y * f );
}

template<typename T>
inline Point2<T> Point2<T>::operator/( const T& f ) const {
  ASSERT( f != 0 );
  Float inv = (Float)1 / f;
  return Point2<T>( x * inv, y * inv );
}

template<typename T>
inline Point2<T>& Point2<T>::operator+=( const Vec2<T>& v ) {
  CHECK_VECTOR( v );
  x += v.x;
  y += v.y;
  return *this;
}

template<typename T>
inline Point2<T>& Point2<T>::operator+=( const Point2<T>& p ) {
  CHECK_POINT( p );
  x += p.x;
  y += p.y;
  return *this;
}

template<typename T>
inline Point2<T>& Point2<T>::operator-=( const Vec2<T>& v ) {
  CHECK_VECTOR( v );
  x -= v.x;
  y -= v.y;
  return *this;
}

template<typename T>
inline Point2<T>& Point2<T>::operator*=( const T& f ) {
  ASSERT( !std::isnan( f ) );
  x *= f;
  y *= f;
  return *this;
}

template<typename T>
inline Point2<T>& Point2<T>::operator/=( const T& f ) {
  ASSERT( f != 0 );
  Float inv = (Float)1 / f;
  x *= inv;
  y *= inv;
  return *this;
}

template<typename T>
inline Point2<T> Point2<T>::operator-( void ) const {
  return Point2<T>( -x, -y );
}

template<typename T>
inline T Point2<T>::operator[]( uint32_t i ) const {
  ASSERT( i <= 1 );
  return ( i == 0 ) ? x : y;
}

template<typename T>
inline T& Point2<T>::operator[]( uint32_t i ) {
  ASSERT( i <= 1 );
  return ( i == 0 ) ? x : y;
}

template<typename T>
inline bool Point2<T>::operator==( const Point2<T>& p ) const {
  return x == p.x && y == p.y;
}

template<typename T>
inline bool Point2<T>::operator!=( const Point2<T>& p ) const {
  return x != p.x || y != p.y;
}

template<typename T>
inline bool Point2<T>::IsNaN( void ) const {
  return std::isnan( x ) || std::isnan( y );
}

template<typename T>
Float DistanceSq( const Point2<T>& p1, const Point2<T>& p2 ) {
  return ( p1 - p2 ).LengthSq();
}

template<typename T>
Float Distance( const Point2<T>& p1, const Point2<T>& p2 ) {
  return ( p1 - p2 ).Length();
}

template<typename T>
Point2<T> Lerp( Float t, const Point2<T>& p1, const Point2<T>& p2 ) {
  return ( (Float)1 - t ) * p1 + t * p2;
}

template<typename T>
Point2<T> Min( const Point2<T>& p1, const Point2<T>& p2 ) {
  return Point2<T>( std::min( p1.x, p2.x ), std::min( p1.x, p2.y ) );
}

template<typename T>
Point2<T> Max( const Point2<T>& p1, const Point2<T>& p2 ) {
  return Point2<T>( std::max( p1.x, p2.x ), std::max( p1.x, p2.y ) );
}

template<typename T>
Point2<T> Floor( const Point2<T>& p ) {
  return Point2<T>( std::floor( p.x ), std::floor( p.y ) );
}

template<typename T>
Point2<T> Ceil( const Point2<T>& p ) {
  return Point2<T>( std::ceil( p.x ), std::ceil( p.y ) );
}

template<typename T>
Point2<T> Abs( const Point2<T>& p ) {
  return Point2<T>( std::abs( p.x ), std::abs( p.y ) );
}

template<typename T>
std::ostream& operator<<( std::ostream& os, const Point2<T>& p ) {
  os << "[Point2: " << p.x << ", " << p.y << "]";
  return os;
}

// point3

template<typename T>
inline Point3<T>::Point3( void ) : x( 0 ), y( 0 ), z( 0 ) {}

template<typename T>
inline Point3<T>::Point3( T _x, T _y, T _z ) : x( _x ), y( _y ), z( _z ) { CHECK_POINT( *this ); }

template<typename T>
inline Point3<T>::Point3( const Point3<T>& p ) : x( p.x ), y( p.y ), z( p.z ) { CHECK_POINT( *this ); }

template<typename T>
template<typename U>
inline Point3<T>::Point3( const Point3<U>& p ) { x = (T)p.x; y = (T)p.y; z = (T)p.z; CHECK_POINT( *this ); }

template<typename T>
template<typename U>
inline Point3<T>::Point3( const Vec3<U>& v ) { x = (T)v.x; y = (T)v.y; z = (T)v.z; CHECK_POINT( *this ); }

template<typename T>
inline Point3<T>::Point3( T v ) : x( v ), y( v ), z( v ) { CHECK_POINT( *this ); }

template<typename T>
inline Point3<T>& Point3<T>::operator=( const Point3<T>& p ) {
  CHECK_POINT( p );
  x = p.x;
  y = p.y;
  z = p.z;
  return *this;
}

template<typename T>
inline Point3<T> Point3<T>::operator+( const Vec3<T>& v ) const {
  CHECK_VECTOR( v );
  return Point3<T>( x + v.x, y + v.y, z + v.z );
}

template<typename T>
inline Point3<T> Point3<T>::operator+( const Point3<T>& p ) const {
  CHECK_POINT( p );
  return Point3<T>( x + p.x, y + p.y, z + p.z );
}

template<typename T>
inline Vec3<T> Point3<T>::operator-( const Point3<T>& p ) const {
  CHECK_POINT( p );
  return Vec3<T>( x - p.x, y - p.y, z - p.z );
}

template<typename T>
inline Point3<T> Point3<T>::operator-( const Vec3<T>& v ) const {
  CHECK_VECTOR( v );
  return Point3<T>( x - v.x, y - v.y, z - v.z );
}

template<typename T>
inline Point3<T> Point3<T>::operator*( const T& f ) const {
  ASSERT( !std::isnan( f ) );
  return Point3<T>( x * f, y * f, z * f );
}

template<typename T>
inline Point3<T> Point3<T>::operator/( const T& f ) const {
  ASSERT( f != 0 );
  Float inv = (Float)1 / f;
  return Point3<T>( x * inv, y * inv, z * inv );
}

template<typename T>
inline Point3<T>& Point3<T>::operator+=( const Vec3<T>& v ) {
  CHECK_VECTOR( v );
  x += v.x;
  y += v.y;
  z += v.z;
  return *this;
}

template<typename T>
inline Point3<T>& Point3<T>::operator+=( const Point3<T>& p ) {
  CHECK_POINT( p );
  x += p.x;
  y += p.y;
  z += p.z;
  return *this;
}

template<typename T>
inline Point3<T>& Point3<T>::operator-=( const Vec3<T>& v ) {
  CHECK_VECTOR( v );
  x -= v.x;
  y -= v.y;
  z -= v.z;
  return *this;
}

template<typename T>
inline Point3<T>& Point3<T>::operator*=( const T& f ) {
  ASSERT( !std::isnan( f ) );
  x *= f;
  y *= f;
  z *= f;
  return *this;
}

template<typename T>
inline Point3<T>& Point3<T>::operator/=( const T& f ) {
  ASSERT( f != 0 );
  Float inv = (Float)1 / f;
  x *= inv;
  y *= inv;
  z *= inv;
  return *this;
}

template<typename T>
inline Point3<T> Point3<T>::operator-( void ) const {
  return Point3<T>( -x, -y, -z );
}

template<typename T>
inline T Point3<T>::operator[]( uint32_t i ) const {
  ASSERT( i <= 2 );
  return ( i == 0 ) ? x : ( i == 1 ) ? y : z;
}

template<typename T>
inline T& Point3<T>::operator[]( uint32_t i ) {
  ASSERT( i <= 2 );
  return ( i == 0 ) ? x : ( i == 1 ) ? y : z;
}

template<typename T>
inline bool Point3<T>::operator==( const Point3<T>& p ) const {
  return x == p.x && y == p.y && z == p.z;
}

template<typename T>
inline bool Point3<T>::operator!=( const Point3<T>& p ) const {
  return x != p.x || y != p.y || z != p.z;
}

template<typename T>
inline bool Point3<T>::IsNaN( void ) const {
  return std::isnan( x ) || std::isnan( y ) || std::isnan( z );
}

template<typename T>
Float DistanceSq( const Point3<T>& p1, const Point3<T>& p2 ) {
  return ( p1 - p2 ).LengthSq();
}

template<typename T>
Float Distance( const Point3<T>& p1, const Point3<T>& p2 ) {
  return ( p1 - p2 ).Length();
}

template<typename T>
Point3<T> Lerp( Float t, const Point3<T>& p1, const Point3<T>& p2 ) {
  return ( (Float)1 - t ) * p1 + t * p2;
}

template<typename T>
Point3<T> Min( const Point3<T>& p1, const Point3<T>& p2 ) {
  return Point3<T>( std::min( p1.x, p2.x ), std::min( p1.x, p2.y ), std::min( p1.z, p2.z ) );
}

template<typename T>
Point3<T> Max( const Point3<T>& p1, const Point3<T>& p2 ) {
  return Point3<T>( std::max( p1.x, p2.x ), std::max( p1.x, p2.y ), std::max( p1.z, p2.z ) );
}

template<typename T>
Point3<T> Floor( const Point3<T>& p ) {
  return Point3<T>( std::floor( p.x ), std::floor( p.y ), std::floor( p.z ) );
}

template<typename T>
Point3<T> Ceil( const Point3<T>& p ) {
  return Point3<T>( std::ceil( p.x ), std::ceil( p.y ), std::ceil( p.z ) );
}

template<typename T>
Point3<T> Abs( const Point3<T>& p ) {
  return Point3<T>( std::abs( p.x ), std::abs( p.y ), std::abs( p.z ) );
}

template<typename T>
std::ostream& operator<<( std::ostream& os, const Point3<T>& p ) {
  os << "[Point3: " << p.x << ", " << p.y << ", " << p.z << "]";
  return os;
}
