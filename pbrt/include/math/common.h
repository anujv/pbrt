#pragma once

#include <cmath>
#include <iostream>
#include <cstdint>
#include <limits>

#include "error.h"

namespace pbrt {

  using Float = float;

  static constexpr Float MaxFloat = std::numeric_limits<Float>::max();
  static constexpr Float Infinity = std::numeric_limits<Float>::infinity();

}
