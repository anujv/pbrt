#pragma once

#include "common.h"
#include "vector.h"

namespace pbrt {

  // point2

  template<typename T>
  struct Point2 {
    T x, y;

    // constructors

    Point2( void );
    Point2( T _x, T _y );
    Point2( const Point2<T>& p );

    template<typename U>
    explicit Point2( const Point2<U>& p );
    template<typename U>
    explicit Point2( const Vec2<U>& v );
    explicit Point2( T v );

    // operators

    Point2<T>& operator=( const Point2<T>& p );

    Point2<T> operator+( const Vec2<T>& v ) const;
    Point2<T> operator+( const Point2<T>& p ) const;
    Vec2<T> operator-( const Point2<T>& p ) const;
    Point2<T> operator-( const Vec2<T>& v ) const;
    Point2<T> operator*( const T& f ) const;
    Point2<T> operator/( const T& f ) const;

    Point2<T>& operator+=( const Vec2<T>& v );
    Point2<T>& operator+=( const Point2<T>& p );
    Point2<T>& operator-=( const Vec2<T>& v );
    Point2<T>& operator*=( const T& f );
    Point2<T>& operator/=( const T& f );

    Point2<T> operator-( void ) const;
    T operator[]( uint32_t i ) const;
    T& operator[]( uint32_t i );

    bool operator==( const Point2<T>& p ) const;
    bool operator!=( const Point2<T>& p ) const;

    bool IsNaN( void ) const;
  };

  template<typename T>
  inline Float DistanceSq( const Point2<T>& p1, const Point2<T>& p2 );

  template<typename T>
  inline Float Distance( const Point2<T>& p1, const Point2<T>& p2 );

  template<typename T>
  inline Point2<T> Lerp( Float t, const Point2<T>& p1, const Point2<T>& p2 );

  template<typename T>
  inline Point2<T> Min( const Point2<T>& p1, const Point2<T>& p2 );

  template<typename T>
  inline Point2<T> Max( const Point2<T>& p1, const Point2<T>& p2 );

  template<typename T>
  inline Point2<T> Floor( const Point2<T>& p );

  template<typename T>
  inline Point2<T> Ceil( const Point2<T>& p );

  template<typename T>
  inline Point2<T> Abs( const Point2<T>& p );

  template<typename T>
  inline std::ostream& operator<<( std::ostream& os, const Point2<T>& p );

  // point3

  template<typename T>
  struct Point3 {
    T x, y, z;

    // constructors

    Point3( void );
    Point3( T _x, T _y, T _z );
    Point3( const Point3<T>& p );

    template<typename U>
    explicit Point3( const Point3<U>& p );
    template<typename U>
    explicit Point3( const Vec3<U>& v );
    explicit Point3( T v );

    // operators

    Point3<T>& operator=( const Point3<T>& p );

    Point3<T> operator+( const Vec3<T>& v ) const;
    Point3<T> operator+( const Point3<T>& p ) const;
    Vec3<T> operator-( const Point3<T>& p ) const;
    Point3<T> operator-( const Vec3<T>& v ) const;
    Point3<T> operator*( const T& f ) const;
    Point3<T> operator/( const T& f ) const;

    Point3<T>& operator+=( const Vec3<T>& v );
    Point3<T>& operator+=( const Point3<T>& p );
    Point3<T>& operator-=( const Vec3<T>& v );
    Point3<T>& operator*=( const T& f );
    Point3<T>& operator/=( const T& f );

    Point3<T> operator-( void ) const;
    T operator[]( uint32_t i ) const;
    T& operator[]( uint32_t i );

    bool operator==( const Point3<T>& p ) const;
    bool operator!=( const Point3<T>& p ) const;

    bool IsNaN( void ) const;
  };

  template<typename T>
  inline Float DistanceSq( const Point3<T>& p1, const Point3<T>& p2 );

  template<typename T>
  inline Float Distance( const Point3<T>& p1, const Point3<T>& p2 );

  template<typename T>
  inline Point3<T> Lerp( Float t, const Point3<T>& p1, const Point3<T>& p2 );

  template<typename T>
  inline Point3<T> Min( const Point3<T>& p1, const Point3<T>& p2 );

  template<typename T>
  inline Point3<T> Max( const Point3<T>& p1, const Point3<T>& p2 );

  template<typename T>
  inline Point3<T> Floor( const Point3<T>& p );

  template<typename T>
  inline Point3<T> Ceil( const Point3<T>& p );

  template<typename T>
  inline Point3<T> Abs( const Point3<T>& p );

  template<typename T>
  inline std::ostream& operator<<( std::ostream& os, const Point3<T>& p );

  #include "point.inl"

  using Point2f = Point2<Float>;
  using Point2i = Point2<int32_t>;
  using Point3f = Point3<Float>;
  using Point3i = Point3<int32_t>;

}
