#pragma once

#include "common.h"

namespace pbrt {

  template<typename T>
  struct Vec3;

  // normal3

  template<typename T>
  struct Normal3 {
    T x, y, z;

    // constructors

    Normal3( void );
    Normal3( T _x, T _y, T _z );
    Normal3( const Normal3<T>& n );
    explicit Normal3( T n );
    explicit Normal3( const Vec3<T>& v );

    // operators

    Normal3<T>& operator=( const Normal3<T>& n );

    Normal3<T> operator+( const Normal3<T>& n ) const;
    Normal3<T> operator-( const Normal3<T>& n ) const;
    Normal3<T> operator*( const T& f ) const;
    Normal3<T> operator/( const T& f ) const;

    Normal3<T>& operator+=( const Normal3<T>& n );
    Normal3<T>& operator-=( const Normal3<T>& n );
    Normal3<T>& operator*=( const T& f );
    Normal3<T>& operator/=( const T& f );

    Normal3<T> operator-( void ) const;
    T operator[]( uint32_t i ) const;
    T& operator[]( uint32_t i );

    bool operator==( const Normal3<T>& n ) const;
    bool operator!=( const Normal3<T>& n ) const;

    Float LengthSq( void ) const;
    Float Length( void ) const;

    bool IsNaN( void ) const;
  };

  template<typename T>
  inline std::ostream& operator<<( std::ostream& os, const Normal3<T>& n );

  template<typename T>
  inline Normal3<T> Abs( const Normal3<T>& n );

  template<typename T>
  inline T Dot( const Normal3<T>& n1, const Normal3<T>& n2 );

  template<typename T>
  inline T AbsDot( const Normal3<T>& n1, const Normal3<T>& n2 );

  template<typename T>
  inline Normal3<T> Cross( const Normal3<T>& n1, const Normal3<T>& n2 );

  template<typename T>
  inline Normal3<T> Normalize( const Normal3<T>& n );

  #include "normal.inl"

  using Normal3f = Normal3<Float>;

}
