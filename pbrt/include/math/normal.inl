
// normal3

template<typename T>
std::ostream& operator<<( std::ostream& os, const Normal3<T>& n ) {
  os << "[Normal3: " << n.x << ", " << n.y << ", " << n.z << "]";
  return os;
}

#define CHECK_NORMAL( N ) ASSERT( !(N).IsNaN() )

template<typename T>
inline Normal3<T>::Normal3( void ) : x( 0 ), y( 0 ), z( 0 ) {}

template<typename T>
inline Normal3<T>::Normal3( T _x, T _y, T _z ) : x( _x ), y( _y ), z( _z ) { CHECK_NORMAL( *this ); }

template<typename T>
inline Normal3<T>::Normal3( const Normal3<T>& n ) : x( n.x ), y( n.y ), z( n.z ) { CHECK_NORMAL( *this ); }

template<typename T>
inline Normal3<T>::Normal3( T n ) : x( n ), y( n ), z( n ) { CHECK_NORMAL( *this ); }

template<typename T>
inline Normal3<T>::Normal3( const Vec3<T>& v ) : x( v.x ), y( v.y ), z( v.z ) { CHECK_NORMAL( *this ); }

template<typename T>
inline Normal3<T>& Normal3<T>::operator=( const Normal3<T>& n ) {
  CHECK_NORMAL( n );
  x = n.x;
  y = n.y;
  z = n.z;
  return *this;
}

template<typename T>
inline Normal3<T> Normal3<T>::operator+( const Normal3<T>& n ) const {
  CHECK_NORMAL( n );
  return Normal3<T>( x + n.x, y + n.y, z + n.z );
}

template<typename T>
inline Normal3<T> Normal3<T>::operator-( const Normal3<T>& n ) const {
  CHECK_NORMAL( n );
  return Normal3<T>( x - n.x, y - n.y, z - n.z );
}

template<typename T>
inline Normal3<T> Normal3<T>::operator*( const T& f ) const {
  ASSERT( !std::isnan( f ) );
  return Normal3<T>( x * f, y * f, z * f );
}

template<typename T>
inline Normal3<T> Normal3<T>::operator/( const T& f ) const {
  ASSERT( f != 0 );
  Float inn = (Float)1 / f;
  return Normal3<T>( x * inn, y * inn, z * inn );
}

template<typename T>
inline Normal3<T>& Normal3<T>::operator+=( const Normal3<T>& n ) {
  CHECK_NORMAL( n );
  x += n.x;
  y += n.y;
  z += n.z;
  return *this;
}

template<typename T>
inline Normal3<T>& Normal3<T>::operator-=( const Normal3<T>& n ) {
  CHECK_NORMAL( n );
  x -= n.x;
  y -= n.y;
  z -= n.z;
  return *this;
}

template<typename T>
inline Normal3<T>& Normal3<T>::operator*=( const T& f ) {
  ASSERT( !std::isnan( f ) );
  x *= f;
  y *= f;
  z *= f;
  return *this;
}

template<typename T>
inline Normal3<T>& Normal3<T>::operator/=( const T& f ) {
  ASSERT( f != 0 );
  Float inn = (Float)1 / f;
  x *= inn;
  y *= inn;
  z *= inn;
  return *this;
}

template<typename T>
inline Normal3<T> Normal3<T>::operator-( void ) const {
  return Normal3<T>( -x, -y, -z );
}

template<typename T>
inline T Normal3<T>::operator[]( uint32_t i ) const {
  ASSERT( i <= 2 );
  return ( i == 0 ) ? x : ( i == 1 ) ? y : z;
}

template<typename T>
inline T& Normal3<T>::operator[]( uint32_t i ) {
  ASSERT( i <= 2 );
  return ( i == 0 ) ? x : ( i == 1 ) ? y : z;
}

template<typename T>
inline bool Normal3<T>::operator==( const Normal3<T>& n ) const {
  return x == n.x && y == n.y && z == n.z;
}

template<typename T>
inline bool Normal3<T>::operator!=( const Normal3<T>& n ) const {
  return x != n.x || y != n.y || z != n.z;
}

template<typename T>
inline Float Normal3<T>::LengthSq( void ) const {
  return x * x + y * y + z * z;
}

template<typename T>
inline Float Normal3<T>::Length( void ) const {
  return std::sqrt( LengthSq() );
}

template<typename T>
inline bool Normal3<T>::IsNaN( void ) const {
  return std::isnan( x ) || std::isnan( y ) || std::isnan( z );
}

template<typename T>
Normal3<T> Abs( const Normal3<T>& n ) {
  return Normal3<T>( std::abs( n.x ), std::abs( n.y ), std::abs( n.z ) );
}

template<typename T>
T Dot( const Normal3<T>& n1, const Normal3<T>& n2 ) {
  return ( n1.x * n2.x ) + ( n1.y * n2.y ) + ( n1.z * n2.z );
}

template<typename T>
T AbsDot( const Normal3<T>& n1, const Normal3<T>& n2 ) {
  return std::abs( Dot( n1, n2 ) );
}

template<typename T>
Normal3<T> Cross( const Normal3<T>& n1, const Normal3<T>& n2 ) {
  double n1x = n1.x, n1y = n1.y, n1z = n1.z;
  double n2x = n1.x, n2y = n1.y, n2z = n1.z;
  return Normal3<T>( n1y * n2z - n1z * n2y,
                     n1z * n2x - n1x * n2z,
                     n1x * n2y - n1y * n2x );
}

template<typename T>
Normal3<T> Normalize( const Normal3<T>& n ) {
  return n / n.Length();
}
