
// vec2

template<typename T>
std::ostream& operator<<( std::ostream& os, const Vec2<T>& v ) {
  os << "[Vec2: " << v.x << ", " << v.y << "]";
  return os;
}

#define CHECK_VECTOR( V ) ASSERT( !(V).IsNaN() )

template<typename T>
inline Vec2<T>::Vec2( void ) : x( 0 ), y( 0 ) {}

template<typename T>
inline Vec2<T>::Vec2( T _x, T _y ) : x( _x ), y( _y ) { CHECK_VECTOR( *this ); }

template<typename T>
inline Vec2<T>::Vec2( const Vec2<T>& v ) : x( v.x ), y( v.y ) { CHECK_VECTOR( *this ); }

template<typename T>
inline Vec2<T>::Vec2( T v ) : x( v ), y( v ) { CHECK_VECTOR( *this ); }

template<typename T>
inline Vec2<T>& Vec2<T>::operator=( const Vec2<T>& v ) {
  CHECK_VECTOR( v );
  x = v.x;
  y = v.y;
  return *this;
}

template<typename T>
inline Vec2<T> Vec2<T>::operator+( const Vec2<T>& v ) const {
  CHECK_VECTOR( v );
  return Vec2<T>( x + v.x, y + v.y );
}

template<typename T>
inline Vec2<T> Vec2<T>::operator-( const Vec2<T>& v ) const {
  CHECK_VECTOR( v );
  return Vec2<T>( x - v.x, y - v.y );
}

template<typename T>
inline Vec2<T> Vec2<T>::operator*( const T& f ) const {
  ASSERT( !std::isnan( f ) );
  return Vec2<T>( x * f, y * f );
}

template<typename T>
inline Vec2<T> Vec2<T>::operator/( const T& f ) const {
  ASSERT( f != 0 );
  Float inv = (Float)1 / f;
  return Vec2<T>( x * inv, y * inv );
}

template<typename T>
inline Vec2<T>& Vec2<T>::operator+=( const Vec2<T>& v ) {
  CHECK_VECTOR( v );
  x += v.x;
  y += v.y;
  return *this;
}

template<typename T>
inline Vec2<T>& Vec2<T>::operator-=( const Vec2<T>& v ) {
  CHECK_VECTOR( v );
  x -= v.x;
  y -= v.y;
  return *this;
}

template<typename T>
inline Vec2<T>& Vec2<T>::operator*=( const T& f ) {
  ASSERT( !std::isnan( f ) );
  x *= f;
  y *= f;
  return *this;
}

template<typename T>
inline Vec2<T>& Vec2<T>::operator/=( const T& f ) {
  ASSERT( f != 0 );
  Float inv = (Float)1 / f;
  x *= inv;
  y *= inv;
  return *this;
}

template<typename T>
inline Vec2<T> Vec2<T>::operator-( void ) const {
  return Vec2<T>( -x, -y );
}

template<typename T>
inline T Vec2<T>::operator[]( uint32_t i ) const {
  ASSERT( i <= 1 );
  return ( i == 0 ) ? x : y;
}

template<typename T>
inline T& Vec2<T>::operator[]( uint32_t i ) {
  ASSERT( i <= 1 );
  return ( i == 0 ) ? x : y;
}

template<typename T>
inline bool Vec2<T>::operator==( const Vec2<T>& v ) const {
  return x == v.x && y == v.y;
}

template<typename T>
inline bool Vec2<T>::operator!=( const Vec2<T>& v ) const {
  return x != v.x || y != v.y;
}

template<typename T>
inline Float Vec2<T>::LengthSq( void ) const {
  return x * x + y * y;
}

template<typename T>
inline Float Vec2<T>::Length( void ) const {
  return std::sqrt( LengthSq() );
}

template<typename T>
inline bool Vec2<T>::IsNaN( void ) const {
  return std::isnan( x ) || std::isnan( y );
}

// vec3

template<typename T>
std::ostream& operator<<( std::ostream& os, const Vec3<T>& v ) {
  os << "[Vec3: " << v.x << ", " << v.y << ", " << v.z << "]";
  return os;
}

template<typename T>
inline Vec3<T>::Vec3( void ) : x( 0 ), y( 0 ), z( 0 ) {}

template<typename T>
inline Vec3<T>::Vec3( T _x, T _y, T _z ) : x( _x ), y( _y ), z( _z ) { CHECK_VECTOR( *this ); }

template<typename T>
inline Vec3<T>::Vec3( const Vec3<T>& v ) : x( v.x ), y( v.y ), z( v.z ) { CHECK_VECTOR( *this ); }

template<typename T>
inline Vec3<T>::Vec3( T v ) : x( v ), y( v ), z( v ) { CHECK_VECTOR( *this ); }

template<typename T>
inline Vec3<T>::Vec3( const Normal3<T>& n ) : x( n.x ), y( n.y ), z( n.z ) { CHECK_VECTOR( *this ); }

template<typename T>
inline Vec3<T>& Vec3<T>::operator=( const Vec3<T>& v ) {
  CHECK_VECTOR( v );
  x = v.x;
  y = v.y;
  z = v.z;
  return *this;
}

template<typename T>
inline Vec3<T> Vec3<T>::operator+( const Vec3<T>& v ) const {
  CHECK_VECTOR( v );
  return Vec3<T>( x + v.x, y + v.y, z + v.z );
}

template<typename T>
inline Vec3<T> Vec3<T>::operator-( const Vec3<T>& v ) const {
  CHECK_VECTOR( v );
  return Vec3<T>( x - v.x, y - v.y, z - v.z );
}

template<typename T>
inline Vec3<T> Vec3<T>::operator*( const T& f ) const {
  ASSERT( !std::isnan( f ) );
  return Vec3<T>( x * f, y * f, z * f );
}

template<typename T>
inline Vec3<T> Vec3<T>::operator/( const T& f ) const {
  ASSERT( f != 0 );
  Float inv = (Float)1 / f;
  return Vec3<T>( x * inv, y * inv, z * inv );
}

template<typename T>
inline Vec3<T>& Vec3<T>::operator+=( const Vec3<T>& v ) {
  CHECK_VECTOR( v );
  x += v.x;
  y += v.y;
  z += v.z;
  return *this;
}

template<typename T>
inline Vec3<T>& Vec3<T>::operator-=( const Vec3<T>& v ) {
  CHECK_VECTOR( v );
  x -= v.x;
  y -= v.y;
  z -= v.z;
  return *this;
}

template<typename T>
inline Vec3<T>& Vec3<T>::operator*=( const T& f ) {
  ASSERT( !std::isnan( f ) );
  x *= f;
  y *= f;
  z *= f;
  return *this;
}

template<typename T>
inline Vec3<T>& Vec3<T>::operator/=( const T& f ) {
  ASSERT( f != 0 );
  Float inv = (Float)1 / f;
  x *= inv;
  y *= inv;
  z *= inv;
  return *this;
}

template<typename T>
inline Vec3<T> Vec3<T>::operator-( void ) const {
  return Vec3<T>( -x, -y, -z );
}

template<typename T>
inline T Vec3<T>::operator[]( uint32_t i ) const {
  ASSERT( i <= 2 );
  return ( i == 0 ) ? x : ( i == 1 ) ? y : z;
}

template<typename T>
inline T& Vec3<T>::operator[]( uint32_t i ) {
  ASSERT( i <= 2 );
  return ( i == 0 ) ? x : ( i == 1 ) ? y : z;
}

template<typename T>
inline bool Vec3<T>::operator==( const Vec3<T>& v ) const {
  return x == v.x && y == v.y && z == v.z;
}

template<typename T>
inline bool Vec3<T>::operator!=( const Vec3<T>& v ) const {
  return x != v.x || y != v.y || z != v.z;
}

template<typename T>
inline Float Vec3<T>::LengthSq( void ) const {
  return x * x + y * y + z * z;
}

template<typename T>
inline Float Vec3<T>::Length( void ) const {
  return std::sqrt( LengthSq() );
}

template<typename T>
inline bool Vec3<T>::IsNaN( void ) const {
  return std::isnan( x ) || std::isnan( y ) || std::isnan( z );
}

template<typename T>
Vec3<T> Abs( const Vec3<T>& v ) {
  return Vec3<T>( std::abs( v.x ), std::abs( v.y ), std::abs( v.z ) );
}

template<typename T>
T Dot( const Vec3<T>& v1, const Vec3<T>& v2 ) {
  return ( v1.x * v2.x ) + ( v1.y * v2.y ) + ( v1.z * v2.z );
}

template<typename T>
T AbsDot( const Vec3<T>& v1, const Vec3<T>& v2 ) {
  return std::abs( Dot( v1, v2 ) );
}

template<typename T>
Vec3<T> Cross( const Vec3<T>& v1, const Vec3<T>& v2 ) {
  double v1x = v1.x, v1y = v1.y, v1z = v1.z;
  double v2x = v1.x, v2y = v1.y, v2z = v1.z;
  return Vec3<T>( v1y * v2z - v1z * v2y,
                  v1z * v2x - v1x * v2z,
                  v1x * v2y - v1y * v2x );
}

template<typename T>
Vec3<T> Normalize( const Vec3<T>& v ) {
  return v / v.Length();
}
