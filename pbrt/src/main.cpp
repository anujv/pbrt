
#include "math/geometry.h"

int main( void ) {

  using namespace pbrt;

  Vec3f v;

  Point3f a( 0, 0, 0 );
  Point3f b( 1, 1, 0 );

  v = b - a;

  Float l = v.LengthSq();

  Normal3f n( v );

  Vec3f pp( n );

  std::cout << n << "\n";
  std::cout << pp << "\n";

  Ray r( a, pp );

  std::cout << r << "\n";

  int asdas = 0;
}
